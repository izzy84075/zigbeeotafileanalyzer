﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZigbeeOTAFileChecker
{
    public partial class Form1 : Form
    {
        string filename = "";

        UInt32 ikeaHeaderSize = 0;
        bool ikeaHeaderFound = false;

        long otaFileStartOffset = 0;

        UInt32 otaFileMagic = 0;
        UInt16 otaHeaderVersion = 0;
        UInt16 otaHeaderLength = 0;
        UInt16 otaHeaderFieldControl = 0;
        UInt16 otaManufacturerCode = 0;
        UInt16 otaImageType = 0;
        UInt32 otaFileVersion = 0;
        UInt16 otaZigbeeStackVersion = 0;
        byte[] otaManufacturerString = new Byte[32];
        UInt32 otaImageSize = 0;
        byte otaSecurityCredentialRequirement = 0;
        UInt64 otaDestinationAddress = 0;
        UInt16 otaMinimumHardwareVersion = 0;
        UInt16 otaMaximumHardwareVersion = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            // Set filter options and filter index.
            //openFileDialog1.Filter = "G+ A3400 4Bit (.sp4)|*.sp4|All Files (*.*)|*.*";
            openFileDialog1.Filter = "All Files|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK.Equals(DialogResult.OK))
            {
                tbxDebug.Text = "";

                // Open the selected file to read.
                System.IO.Stream fileStream = openFileDialog1.OpenFile();
                filename = openFileDialog1.FileName;
                ikeaHeaderFound = false;
                tbxDebug.Text += "File: " + openFileDialog1.FileName + "\r\n";

                parseFile(fileStream);

                fileStream.Close();
            }
        }

        private void parseFile(System.IO.Stream fileStream)
        {
            string tempText = "";

            tbxFilename.Text = filename;
            tbxManufacturer.Text = "";
            tbxImageType.Text = "";
            tbxFileVersion.Text = "";
            tbxImageSize.Text = "";
            tbxManufString.Text = "";
            tbxSecurityCredentials.Text = "";
            tbxDeviceSpecific.Text = "";
            tbxHardwareVersions.Text = "";

            tempText += "Parsing file header.\r\n";
            using (System.IO.BinaryReader reader = new System.IO.BinaryReader(fileStream))
            {
                otaFileMagic = reader.ReadUInt32();

                if(otaFileMagic == 0x5349474E)
                {
                    //IKEA firmware container file marker
                    tempText += "\tIKEA firmware container found.\r\n";
                    ikeaHeaderFound = true;

                    tempText += "\t\tSkipping to end of IKEA header...\r\n";
                    reader.BaseStream.Seek(8, System.IO.SeekOrigin.Current);
                    ikeaHeaderSize = reader.ReadUInt32();
                    reader.BaseStream.Seek((ikeaHeaderSize - 4), System.IO.SeekOrigin.Current);
                    tempText += "\t\tProbable start of Zigbee OTA header offset: 0x" + reader.BaseStream.Position.ToString("X") + "\r\n";

                    //Read in what should be the new magic
                    otaFileMagic = reader.ReadUInt32();
                }

                if (otaFileMagic == 0x0BEEF11E)
                {
                    //Standard Zigbee OTA file marker
                    tempText += "\tValid OTA file magic bytes.\r\n";
                    otaFileStartOffset = (reader.BaseStream.Position - 4);

                    tempText += "\tParsing required header fields...\r\n";

                    //Read in the rest of the header
                    otaHeaderVersion = reader.ReadUInt16();
                    otaHeaderLength = reader.ReadUInt16();
                    otaHeaderFieldControl = reader.ReadUInt16();
                    otaManufacturerCode = reader.ReadUInt16();
                    otaImageType = reader.ReadUInt16();
                    otaFileVersion = reader.ReadUInt32();
                    otaZigbeeStackVersion = reader.ReadUInt16();
                    otaManufacturerString = reader.ReadBytes(32);
                    otaImageSize = reader.ReadUInt32();

                    //Fill in some boxes
                    tbxManufacturer.Text = "0x" + otaManufacturerCode.ToString("X").PadLeft(4, '0');
                    tbxImageType.Text = "0x" + otaImageType.ToString("X").PadLeft(4, '0');
                    if (otaImageType < 0xFFC0)
                    {
                        tbxImageType.Text += ", Manufacturer Specific.";
                    } else if (otaImageType == 0xFFC0)
                    {
                        tbxImageType.Text += ", Client Security Credentials.";
                    } else if (otaImageType == 0xFFC1)
                    {
                        tbxImageType.Text += ", Client Configuration.";
                    } else if (otaImageType == 0xFFC2)
                    {
                        tbxImageType.Text += ", Server Log.";
                    } else if (otaImageType == 0xFFC3)
                    {
                        tbxImageType.Text += ", Picture.";
                    } else if(otaImageType == 0xFFFF)
                    {
                        tbxImageType.Text += ", Wildcard.";
                    } else
                    {
                        tbxImageType.Text += ", Invalid Image Type.";
                    }
                    tbxFileVersion.Text = "0x" + otaFileVersion.ToString("X").PadLeft(8, '0');
                    tbxImageSize.Text = "0x" + otaImageSize.ToString("X").PadLeft(8, '0');
                    tbxManufString.Text = Encoding.UTF8.GetString(otaManufacturerString, 0, otaManufacturerString.Length);

                    tempText += "\tRequired header fields parsed.\r\n";

                    tempText += "\tChecking for additional header fields...\r\n";

                    if( (otaHeaderFieldControl & 0x01) == 0x01)
                    {
                        tempText += "\tSecurity Credential Version field present.\r\n";
                        
                        otaSecurityCredentialRequirement = reader.ReadByte();
                        switch(otaSecurityCredentialRequirement)
                        {
                            case 0x00:
                                tbxSecurityCredentials.Text = "SE 1.0";
                                break;
                            case 0x01:
                                tbxSecurityCredentials.Text = "SE 1.1";
                                break;
                            case 0x02:
                                tbxSecurityCredentials.Text = "SE 2.0";
                                break;
                            case 0x03:
                                tbxSecurityCredentials.Text = "SE 1.2";
                                break;
                            default:
                                tbxSecurityCredentials.Text = "ERROR, 0x" + otaSecurityCredentialRequirement.ToString("X").PadLeft(2, '0');
                                break;
                        }
                    } else
                    {
                        tempText += "\tSecurity Credential Version field not present.\r\n";
                        tbxSecurityCredentials.Text = "Not Specified";
                    }

                    if( (otaHeaderFieldControl & 0x02) == 0x02)
                    {
                        tempText += "\tUpgrade File Destination field present.\r\n";

                        otaDestinationAddress = reader.ReadUInt64();
                        tbxDeviceSpecific.Text = "Yes, 0x" + otaDestinationAddress.ToString("X").PadLeft(16, '0');
                    } else
                    {
                        tempText += "\tUpgrade File Destination field not present.\r\n";
                        tbxDeviceSpecific.Text = "No";
                    }

                    if( (otaHeaderFieldControl & 0x04) == 0x04)
                    {
                        tempText += "\tHardware Versions fields present.\r\n";
                        otaMinimumHardwareVersion = reader.ReadUInt16();
                        otaMaximumHardwareVersion = reader.ReadUInt16();

                        tbxHardwareVersions.Text = "Min: 0x" + otaMinimumHardwareVersion.ToString("X").PadLeft(4, '0') + ", Max: 0x" + otaMaximumHardwareVersion.ToString("X").PadLeft(4, '0');
                    } else
                    {
                        tempText += "\tHardware Versions fields not present.\r\n";

                        tbxHardwareVersions.Text = "Not specified";
                    }

                    tempText += "\tHeader parsing complete.\r\n";

                    if(cbxExtract.Checked && ikeaHeaderFound)
                    {
                        tempText += "OTA extraction requested, and needed. Extracting!\r\n";

                        string tempFilename = filename + ".ota";

                        System.IO.FileStream outputFileStream = File.Open(tempFilename, FileMode.Create);
                        BinaryWriter writer = new BinaryWriter(outputFileStream);

                        reader.BaseStream.Position = otaFileStartOffset;

                        byte[] buffer = new Byte[1024];
                        int bytesRead;

                        // while the read method returns bytes
                        // keep writing them to the output stream
                        while ((bytesRead =
                                reader.Read(buffer, 0, 1024)) > 0)
                        {
                            writer.Write(buffer, 0, bytesRead);
                        }

                        writer.Close();

                        tempText += "\tOTA extraction complete. See " + tempFilename + " for the resulting file.\r\n";
                    }


                }
                else
                {
                    tempText += "\tInvalid OTA file magic bytes.\r\n";
                    tempText += "\tNot attempting more parsing currently.\r\n";
                }
            }

            tbxDebug.Text += tempText;
        }
    }
}
