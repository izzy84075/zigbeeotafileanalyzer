﻿namespace ZigbeeOTAFileChecker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.cbxExtract = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxManufacturer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxImageType = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxFileVersion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxManufString = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxImageSize = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxHardwareVersions = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxDeviceSpecific = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxSecurityCredentials = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxFilename = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbxDebug = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(13, 13);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Load File";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // cbxExtract
            // 
            this.cbxExtract.AutoSize = true;
            this.cbxExtract.Checked = true;
            this.cbxExtract.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxExtract.Location = new System.Drawing.Point(12, 42);
            this.cbxExtract.Name = "cbxExtract";
            this.cbxExtract.Size = new System.Drawing.Size(133, 17);
            this.cbxExtract.TabIndex = 1;
            this.cbxExtract.Text = "Extract OTA if possible";
            this.cbxExtract.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Manufacturer:";
            // 
            // tbxManufacturer
            // 
            this.tbxManufacturer.Location = new System.Drawing.Point(85, 68);
            this.tbxManufacturer.Name = "tbxManufacturer";
            this.tbxManufacturer.ReadOnly = true;
            this.tbxManufacturer.Size = new System.Drawing.Size(44, 20);
            this.tbxManufacturer.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Image Type";
            // 
            // tbxImageType
            // 
            this.tbxImageType.Location = new System.Drawing.Point(75, 94);
            this.tbxImageType.Name = "tbxImageType";
            this.tbxImageType.ReadOnly = true;
            this.tbxImageType.Size = new System.Drawing.Size(233, 20);
            this.tbxImageType.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(305, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "File Version:";
            // 
            // tbxFileVersion
            // 
            this.tbxFileVersion.Location = new System.Drawing.Point(375, 68);
            this.tbxFileVersion.Name = "tbxFileVersion";
            this.tbxFileVersion.ReadOnly = true;
            this.tbxFileVersion.Size = new System.Drawing.Size(67, 20);
            this.tbxFileVersion.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Manufacturer String:";
            // 
            // tbxManufString
            // 
            this.tbxManufString.Location = new System.Drawing.Point(115, 42);
            this.tbxManufString.Name = "tbxManufString";
            this.tbxManufString.ReadOnly = true;
            this.tbxManufString.Size = new System.Drawing.Size(441, 20);
            this.tbxManufString.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(135, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Total Image Size:";
            // 
            // tbxImageSize
            // 
            this.tbxImageSize.Location = new System.Drawing.Point(230, 68);
            this.tbxImageSize.Name = "tbxImageSize";
            this.tbxImageSize.ReadOnly = true;
            this.tbxImageSize.Size = new System.Drawing.Size(69, 20);
            this.tbxImageSize.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxHardwareVersions);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbxDeviceSpecific);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbxSecurityCredentials);
            this.groupBox1.Controls.Add(this.tbxImageType);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbxFilename);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbxImageSize);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxManufString);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbxFileVersion);
            this.groupBox1.Controls.Add(this.tbxManufacturer);
            this.groupBox1.Location = new System.Drawing.Point(155, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 154);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Loaded File Details";
            // 
            // tbxHardwareVersions
            // 
            this.tbxHardwareVersions.Location = new System.Drawing.Point(392, 120);
            this.tbxHardwareVersions.Name = "tbxHardwareVersions";
            this.tbxHardwareVersions.ReadOnly = true;
            this.tbxHardwareVersions.Size = new System.Drawing.Size(164, 20);
            this.tbxHardwareVersions.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(241, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(145, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Required Hardware Versions:";
            // 
            // tbxDeviceSpecific
            // 
            this.tbxDeviceSpecific.Location = new System.Drawing.Point(135, 120);
            this.tbxDeviceSpecific.Name = "tbxDeviceSpecific";
            this.tbxDeviceSpecific.ReadOnly = true;
            this.tbxDeviceSpecific.Size = new System.Drawing.Size(100, 20);
            this.tbxDeviceSpecific.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Device-Specific Update:";
            // 
            // tbxSecurityCredentials
            // 
            this.tbxSecurityCredentials.Location = new System.Drawing.Point(456, 94);
            this.tbxSecurityCredentials.Name = "tbxSecurityCredentials";
            this.tbxSecurityCredentials.ReadOnly = true;
            this.tbxSecurityCredentials.Size = new System.Drawing.Size(100, 20);
            this.tbxSecurityCredentials.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(314, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Security Credential Version:";
            // 
            // tbxFilename
            // 
            this.tbxFilename.Location = new System.Drawing.Point(69, 16);
            this.tbxFilename.Name = "tbxFilename";
            this.tbxFilename.ReadOnly = true;
            this.tbxFilename.Size = new System.Drawing.Size(487, 20);
            this.tbxFilename.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "File Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxDebug);
            this.groupBox2.Location = new System.Drawing.Point(155, 173);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(562, 265);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Extra Info";
            // 
            // tbxDebug
            // 
            this.tbxDebug.Location = new System.Drawing.Point(7, 20);
            this.tbxDebug.Multiline = true;
            this.tbxDebug.Name = "tbxDebug";
            this.tbxDebug.ReadOnly = true;
            this.tbxDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxDebug.Size = new System.Drawing.Size(549, 239);
            this.tbxDebug.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbxExtract);
            this.Controls.Add(this.btnLoad);
            this.Name = "Form1";
            this.Text = "Zigbee OTA File Analyzer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.CheckBox cbxExtract;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxManufacturer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxImageType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxFileVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxManufString;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxImageSize;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxHardwareVersions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxDeviceSpecific;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbxSecurityCredentials;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxFilename;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbxDebug;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

